<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="es.unex.mydai.persistence.vo.Users"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body> 
	<form:form modelAttribute="usuario" action="login">
		<fieldset>
			<legend> Iniciar Sesion </legend>
			<label for="emai">Correo</label>
			<form:input path="email" type="email" name="email" id="email" size="30" maxlength="150" value="emilio@emilio.com" required="required"/>
			<label for="pas">Contrase�a</label>
			<form:input path="pass" type="password" name="pass"	id="pass" value="123" size="30" maxlength="250" required="required"/>
		</fieldset>
		<br />
		<br />
		<h2>
			<c:if test="${mensaje!=null}">${mensaje}</c:if>
		</h2>
		<input type="submit" name="Iniciar Sesion" value="Iniciar Sesion" />
		
	</form:form>
	<br />
	<form action="anadirUsuario">
		<input type="submit" name="anadirUsuario" value="A�adir Usuario">
	</form>
</body>
</html>