package es.unex.mydai.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Users;
import es.unex.mydai.persistence.vo.Visitas;
import es.unex.mydai.service.HashtagService;
import es.unex.mydai.service.UsersService;
import es.unex.mydai.service.VisitaService;

@Controller
public class VisitaController {
	private final UsersService userService;
	private final VisitaService visitaService;
	private final HashtagService hashtagService;
	
	@Autowired
	public VisitaController(VisitaService visitaService, UsersService userService,HashtagService hashtagService) {
		this.visitaService = visitaService;
		this.userService = userService;
		this.hashtagService= hashtagService;
	}
	
	@RequestMapping(value = "/{idUsers}/anadirVisita", method = RequestMethod.GET)
	public String anadirUsuario(ModelMap model) {
		Users u = new Users();
		model.addAttribute("u", u);
		model.addAttribute("usuario", u);
		Visitas v= new Visitas();
		model.addAttribute("v",v);
		return "anadirVisita";
	}

	@RequestMapping(value = "/{idUsers}/anadirVisita", method = RequestMethod.POST)
	public String insertarUsuario(ModelMap model, @ModelAttribute("usuario") Users us,@PathVariable("idUsers") long id) {
			try {
			Users usuarioPerfil = this.userService.findUserById(id);
			model.addAttribute("user", usuarioPerfil);
			Users usuarioVisitado = this.userService.findUserById(us.getIdUsers());
			model.addAttribute("users", usuarioVisitado);
			List <Hashtags>has=this.hashtagService.busqueda(usuarioVisitado.getIdUsers());
			model.addAttribute("hashtags", has);
			Visitas v=new Visitas();
			v.setIdUsuarioVisitado(usuarioVisitado.getIdUsers());
			v.setIdUsuarioVisitante(usuarioPerfil.getIdUsers());
		 	this.visitaService.createVisita(v);
		 	
		 return "verPerfil";

			} catch (Exception e) {
				model.addAttribute("mensaje", "Problema visitando el perfil");
				return "menu";
}

	}
	@RequestMapping(value = "/{idUsers}/mostrarVisita", method = RequestMethod.GET)
	public String mostrarVisita(ModelMap model,@PathVariable("idUsers") long id) {

		Users usuario= this.userService.findUserById(id);

		ArrayList<Visitas> v=this.visitaService.busqueda(id);

		ArrayList<Users> listaUsuarios=new ArrayList<Users>();

		for(int i=0;i<v.size();i++) {
			Users u=this.userService.findUserById(v.get(i).getIdUsuarioVisitante());
			listaUsuarios.add(u);
		}
		model.addAttribute("users",listaUsuarios);
		 return "mostrarVisita";

	
	
	}


}
