package es.unex.mydai.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Users;
@Component
@Transactional
public class HashtagDAOImpl extends GenericDAOImpl<Hashtags, Long> implements HashtagDAO{

	@PersistenceContext
	protected EntityManager entityManager;
	public HashtagDAOImpl() {
		this.setEntityClass(Hashtags.class);
	}
	public ArrayList<Hashtags> busqueda(long id) {
		ArrayList<Hashtags> a= (ArrayList<Hashtags>) this.entityManager.createQuery("from Hashtags where user_id='"+id+"'",Hashtags.class).getResultList();
		return a; 
	}
	public List<Hashtags> todosHashtags() {
		List<Hashtags> a=  this.entityManager.createQuery("from Hashtags",Hashtags.class).getResultList();
		return a; 
	}


}
