package es.unex.mydai.dao;

import java.util.List;

import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Publicacion;

public interface PublicacionDAO extends GenericDAO<Publicacion, Long>{
	List <Publicacion> todasPublicaciones();
}

