package es.unex.mydai.dao;

import java.util.ArrayList;
import java.util.List;

import es.unex.mydai.persistence.vo.Hashtags;

public interface HashtagDAO extends GenericDAO<Hashtags, Long> {
	ArrayList<Hashtags> busqueda(long id) ;
	List<Hashtags> todosHashtags() ;
}
