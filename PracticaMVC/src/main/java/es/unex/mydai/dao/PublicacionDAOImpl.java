package es.unex.mydai.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Publicacion;
import es.unex.mydai.persistence.vo.Users;

@Component
@Transactional
public class PublicacionDAOImpl extends GenericDAOImpl<Publicacion, Long> implements PublicacionDAO{
	@PersistenceContext
	protected EntityManager entityManager;
	public PublicacionDAOImpl() {
		this.setEntityClass(Publicacion.class);
	}
	
	public List<Publicacion> todasPublicaciones() {
		List<Publicacion> m=  this.entityManager.createQuery("from Publicacion",Publicacion.class).getResultList();
		return m; 
	}
//
//	public List<Publicacion> busqueda(long id) {
//		List<Publicacion> m=  this.entityManager.createQuery("from Mensajes where idUsuarioDestino="+id,Publicacion.class).getResultList();
//		return m; 
//	}
}
