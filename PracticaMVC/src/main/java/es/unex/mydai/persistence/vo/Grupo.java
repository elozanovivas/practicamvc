package es.unex.mydai.persistence.vo;

import java.io.Serializable;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.JoinColumn;


@Entity
@Table(name="GRUPO")
public class Grupo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="idgrupo")
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private long idGrupo;
	
	@Column(name="nombre_grupo")
	private String nombreGrupo;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USUARIOS_GRUPO", joinColumns = { @JoinColumn(name = "idgrupo") }, inverseJoinColumns = {
			@JoinColumn(name = "idusers") })
	private List <Users>usuariosLista;
	
	public Grupo() {
		super();
	}

	public Grupo(long idGrupo, String nombreGrupo, List<Users> usuariosLista) {
		super();
		this.idGrupo = idGrupo;
		this.nombreGrupo = nombreGrupo;
		this.usuariosLista = usuariosLista;
	}

	public long getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(long idGrupo) {
		this.idGrupo = idGrupo;
	}

	public String getNombreGrupo() {
		return nombreGrupo;
	}

	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}

	public List<Users> getUsuariosLista() {
		return usuariosLista;
	}

	public void setUsuariosLista(List<Users> usuariosLista) {
		this.usuariosLista = usuariosLista;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Grupo [idGrupo=" + idGrupo + ", nombreGrupo=" + nombreGrupo + ", usuariosLista=" + usuariosLista + "]";
	}
	
	
	
	
}
