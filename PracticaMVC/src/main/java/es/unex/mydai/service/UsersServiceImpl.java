package es.unex.mydai.service;

import java.util.ArrayList;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.unex.mydai.dao.UsersDAO;
import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Users;

@Component
public class UsersServiceImpl implements UsersService {

	@Autowired
	private UsersDAO usersDAO;

	public Users createUser(Users user) {
		return usersDAO.create(user);
	}

	public Users findUserById(long id) {
		return usersDAO.read(id);
	}

	public Users updateUser(Users user) {
		return usersDAO.update(user);
	}

	public void deleteUser(Users user) {
		usersDAO.delete(user);
	}
	public Users findUserbyname(String user) {
		return this.usersDAO.busqueda(user);
	}
	public Users loguearse(Users u) {
		return this.usersDAO.loguearse(u);
	}
	
	public List<Users> todosUsuarios() {
		return this.usersDAO.todosUsuarios(); 
	}

}
