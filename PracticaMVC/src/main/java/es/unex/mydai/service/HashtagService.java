package es.unex.mydai.service;

import java.util.ArrayList;
import java.util.List;

import es.unex.mydai.persistence.vo.Hashtags;

public interface HashtagService {
	
	public Hashtags createHashtag(Hashtags hashtag);
	
	public Hashtags findById(long id);
	
	public Hashtags updateHashtag(Hashtags h);
	
	public void deleteHashtag(Hashtags h);
	
	public ArrayList<Hashtags>busqueda (long id);
	
	public List<Hashtags>todosHashtags ();
}
