package es.unex.mydai.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import es.unex.mydai.dao.MensajesDAO;
import es.unex.mydai.dao.PublicacionDAO;
import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Mensajes;
import es.unex.mydai.persistence.vo.Publicacion;
@Component
public class PublicacionServiceImpl implements PublicacionService {

	@Autowired
	private PublicacionDAO publicacionDAO;
	
	public Publicacion createPublicacion(Publicacion publicacion) {
		return publicacionDAO.create(publicacion);
	}

	public Publicacion findPublicacionById(long id) {
		return publicacionDAO.read(id);
	}

	public Publicacion updatePublicacion(Publicacion publicacion) {
		return publicacionDAO.update(publicacion);
	}

	public void deletePublicacion(Publicacion publicacion) {
		publicacionDAO.delete(publicacion);
	}
	public List<Publicacion> todasPublicaciones() {
		return publicacionDAO.todasPublicaciones();
	}
//	public List<Publicacion> busqueda(long id) {
//		return mensajesDAO.busqueda(id);
//	}
	
}
