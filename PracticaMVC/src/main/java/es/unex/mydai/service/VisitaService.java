package es.unex.mydai.service;

import java.util.ArrayList;

import es.unex.mydai.persistence.vo.Hashtags;
import es.unex.mydai.persistence.vo.Visitas;

public interface VisitaService {
	
	public Visitas createVisita(Visitas hashtag);
	
	public Visitas findById(long id);
	
	public Visitas updateVisita(Visitas h);
	
	public void deleteVisita(Visitas h);
	
	public ArrayList<Visitas>busqueda (long id);
}
